#! /usr/bin/python3

from os import environ
from testcontainers.core.container import DockerContainer
from time import sleep
from testutils import retry_with_timeout
import re

image_name=environ.get('TEST_IMAGE', default = None)
hex_pattern = re.compile("0x[0-9]+")

def get_jriver_window_id(container: DockerContainer):
    timeout = 60
    while timeout > 0:
        result = container.exec("wmctrl -l")
        stdout = str(result.output)
        if result.exit_code == 0 and stdout.count("JRiver Media Center") == 1:
            for line in stdout.splitlines():
                fields = line.strip().split()
                if len(fields) > 5 and fields[3] == "JRiver" and fields[4] == "Media" and fields[5] == "Center":
                    # field[0] returns something like b'0x00200158, extract hex number
                    match = hex_pattern.search(fields[0])
                    if match: return match.group()
        sleep(1)
        timeout -= 1

def is_jriver_window_visible(container: DockerContainer, windowId) -> bool:
    result = container.exec(f"xprop -id {windowId}")
    if result.exit_code != 0: return False
    states=None
    for line in str(result.output).splitlines():
        if "_NET_WM_STATE(ATOM)" in line:
            states=line
            break
    if not states: return False
    return states.count("_NET_WM_STATE_HIDDEN") == 0

def test_jriver_window_exists():
    """
    Tests if MC Window exists at all.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0") as container:
        window_id = get_jriver_window_id(container)
        assert window_id is not None


def test_jriver_window_visible():
    """
    Tests if MC Window is Visible on startup.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0") as container:
        window_id = get_jriver_window_id(container)
        assert window_id is not None, "Window does not exist"

        is_visible = retry_with_timeout(lambda: is_jriver_window_visible(container, window_id))
        assert is_visible == True, "Window not visible"

def test_jriver_window_minimize():
    """
    Tests if MC Window comes back up when minimized.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0") as container:
        window_id = get_jriver_window_id(container)

        for _ in range(10):
            is_visible = retry_with_timeout(lambda: is_jriver_window_visible(container, window_id))
            assert is_visible == True, "Window not visible"

            container.exec(f"xdotool windowactivate {window_id}")
            container.exec("xdotool getactivewindow windowminimize")

