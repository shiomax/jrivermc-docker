#!/command/with-contenv /bin/sh

set -u # Treat unset variables as an error.

echo "Starting tigervnc..."

UNIX_SOCKET="/tmp/tigervnc.sock"

# Remove Socket file
if [ -f $UNIX_SOCKET ]; then
    rm $UNIX_SOCKET
fi

# Enable VNC Server on Port 5900 if enabled
case "${VNC_LISTEN:-<unset>}" in
    LOCALHOST)
    # Use port 5900, but restrict access to localhost
    EXTRA_ARGS="-rfbport 5900 -localhost 1"
    ;;
    ALLOW_ALL)
    # Use port 5900 localhost 0 to accept all connections
    EXTRA_ARGS="-rfbport 5900 -localhost 0"
    ;;
    *)
    # Start with unix socket, no port
    EXTRA_ARGS="-rfbport -1 -localhost 1 -rfbunixpath $UNIX_SOCKET -rfbunixmode 0666"
    ;;
esac

exec 2>&1 Xvnc $DISPLAY \
    -geometry ${DISPLAY_WIDTH}x${DISPLAY_HEIGHT} \
    -depth 24 \
    -desktop x11 \
    -IdleTimeout 0 \
    -MaxDisconnectionTime 0 \
    -MaxConnectionTime 0 \
    -MaxIdleTime 0 \
    -SecurityTypes None $EXTRA_ARGS
