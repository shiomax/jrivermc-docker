#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

# Defaults
SCRIPT_DIR="$(readlink -f "$(dirname "$0")")"
DEB_URL=repository

$SCRIPT_DIR/scripts/check-dependencies.sh

usage() {
    if [ "$*" ]; then
        echo "$*"
        echo
    fi

    echo "usage: $( basename $0 ) [OPTIONS]... BUILD_VARIANT

Arguments:
  BUILD_VARIANT         Defines the container flavor to build.  Valid values are:
$(find "$SCRIPT_DIR"/versions -type f | awk -F '/' '{print "                        "$NF}' | sort)

Options:
  -h, --help               Display this help.
  --registry <name>        Dockerhub Registry (default: shiomax/jrivermc, shiomax/jrivermc-testing)
  --no-cache               Disable Docker build cache.
  --export <registry>      Export image to a registry and write metadata file.
  --deb-url <url>          Url of .deb file to use for build.
  --cleanup                Cleanup images before and after build.
  --disable-buildkit       By default DOCKER_BUILDKIT is set to 1, this disables that behaviour.
  --jriver-tag <tag>       Required when building version from repository (latest or stable).
  --use-buildx             Use buildx to build docker container.
  --buildx-builder <name>  Specify buildx builder to use by name.
  --buildx-arch <arch>     Buildx Architecture to build (linux/amd64, linux/arm64 or linux/arm/v7).
"
}

# Determines deb url to use from CI variables
get_ci_deb_url() {
    DOCKER_INFO_ARCH=$(docker info --format '{{json .}}' | jq --raw-output '.Architecture')
    case $DOCKER_INFO_ARCH in
        x86_64)
            echo "${CI_DEB_URL_AMD64:-unset}"
        ;;
        aarch64)
            echo "${CI_DEB_URL_ARM64:-unset}"
        ;;
        *)
            echo "Unsupported Platform $DOCKER_ARCH."
            exit 1;
        ;;
    esac
}

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        --registry)
            shift
            DOCKER_REPO="$1"
            ;;
        --no-cache)
            DISABLE_CACHE=1
            ;;
        --export)
            EXPORT_IMAGE=1
            shift
            EXPORT_REGISTRY="$1"
            ;;
        --deb-url)
            shift
            DEB_URL="$1"
            ;;
        --cleanup)
            RUN_CLEANUP=1
            ;;
        --disable-buildkit)
            USE_BUILDKIT=0
            ;;
        --jriver-tag)
            shift
            JRIVER_TAG="$1"
            ;;
        --use-buildx)
            USE_BUILDX=1
            ;;
        --buildx-builder)
            shift
            BUILDX_BUILDER_NAME="$1"
            ;;
        --buildx-arch)
            shift
            BUILDX_BUILDER_ARCH="$1"
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

if [ "${CI_DEB_URL_AMD64:-unset}" != "unset" ]; then
    echo "Determining deb url from CI variables..."
    DEB_URL="$(get_ci_deb_url)"
fi

if [ "$DEB_URL" = "repository" ] && ! [[ "${JRIVER_TAG:-unset}" =~ (latest|stable) ]]; then
    echo "Valid values for --jriver-tag are 'latest' and 'stable' only."
    exit 1
fi

if [ "${USE_BUILDX:-0}" -eq 1 ] && ! [[ "${BUILDX_BUILDER_ARCH:-unset}" =~ (linux/amd64|linux/arm64|linux/arm/v7) ]]; then
    echo "Buildx architecture should be one of linux/amd64, linux/arm64 or linux/arm/v7."
    exit 1
fi

BUILD_VARIANT="${1:-unset}"

[ ! "$BUILD_VARIANT" = "unset" ] || { usage "ERROR: Docker tag not supplied."; exit 1; }
if [ ! -f "$SCRIPT_DIR"/versions/"$BUILD_VARIANT" ]; then
    usage "ERROR: Invalid docker tag: $BUILD_VARIANT."
    exit 1
fi

# Export build variables.
IMAGE_VERSION=$(./scripts/get-image-version.sh)

source versions/"$BUILD_VARIANT"

# Determine release docker repository
DOCKER_REPO="${CI_RELEASE_REPOSITORY:-shiomax/jrivermc-testing}"

# Set buildkit environment variable
if [ ${USE_BUILDKIT:-1} -eq 1 ]; then
    export DOCKER_BUILDKIT=1
else
    unset DOCKER_BUILDKIT
fi

print_build_info() {
    echo "--------------------------------"
    echo "Starting build"
    echo "--------------------------------"
    echo "REPOSITORY:          $DOCKER_REPO"
    echo "APT_REPOSITORY:      $APT_REPOSITORY"
    echo "BUILD_VARIANT:       $BUILD_VARIANT"
    echo "BASEIMAGE:           $BASEIMAGE"
    echo "IMAGE_VERSION:       $IMAGE_VERSION"
    echo "CACHE DISABLED:      ${DISABLE_CACHE-0}"
    echo "JRIVER_VERSION:      $JRIVER_RELEASE"
    echo "JRIVER_TAG:          $JRIVER_TAG"
    echo "EXPORT_IMAGE:        ${EXPORT_IMAGE-0}"
    echo "EXPORT_REGISTRY:     ${EXPORT_REGISTRY:-<unset>}"
    echo "DEB_URL:             $DEB_URL"
    echo "RUN CLEANUP:         ${RUN_CLEANUP-0}"
    echo "USE_BUILDX:          ${USE_BUILDX-0}"
    echo "BUILDX_BUILDER_ARCH: ${BUILDX_BUILDER_ARCH:-<unset>}"
    echo "BUILDX_BUILDER_NAME: ${BUILDX_BUILDER_NAME:-jrmcbuilder}"
    echo "--------------------------------"
}

# Determine tragetplatform
get_targetplatform() {
    if [ "${USE_BUILDX:-0}" -eq 1 ]; then
        echo "$BUILDX_BUILDER_ARCH"
    else
        DOCKER_INFO_ARCH=$(docker info --format '{{json .}}' | jq --raw-output '.Architecture')
        case $DOCKER_INFO_ARCH in
            x86_64)
                echo "linux/amd64"
            ;;
            armv7l)
                echo "linux/arm/v7"
            ;;
            aarch64)
                echo "linux/arm64"
            ;;
            *)
                >&2 echo "Unsupported Platform $DOCKER_INFO_ARCH."
                exit 1;
            ;;
        esac
    fi
}

# Determine architecture tag for image tag
get_arch_tag() {
    TARGETPLATFORM="$(get_targetplatform)"

    case $TARGETPLATFORM in
        linux/amd64)
            echo "amd64"
        ;;
        linux/arm64)
            echo "arm64"
        ;;
        linux/arm/v7)
            echo "armv7"
        ;;
        *)
            >&2 echo "Unsupported buildx architecture $TARGETPLATFORM."
            exit 1;
        ;;
    esac
}

# Get Docker tag for image build
get_docker_tag() {
    ARCH_TAG="$(get_arch_tag)"
    DOCKER_TAG="$DOCKER_REPO:$BUILD_VARIANT-$IMAGE_VERSION-$ARCH_TAG"
    echo "$DOCKER_TAG"
}

# Build container using docker buildx
build_container_buildx() {
    DOCKER_TAG="$(get_docker_tag)"
    TARGETPLATFORM="$(get_targetplatform)"

    BUILDX_BUILDER_NAME="${BUILDX_BUILDER_NAME:-jrmcbuilder}"
    echo "Using buildx builder '$BUILDX_BUILDER_NAME' to build container."

    # Create builder if it does not exist
    $(docker buildx inspect "$BUILDX_BUILDER_NAME" > /dev/null) \
        || { \
            echo "Builder $BUILDX_BUILDER_NAME does not exist. Creating it."; \
            docker buildx create \
                --name "$BUILDX_BUILDER_NAME"; }

    docker buildx use "$BUILDX_BUILDER_NAME"

    EXTRA_ARGS=""
    [ ${DISABLE_CACHE:-0} -eq 1 ] && EXTRA_ARGS="--no-cache --pull" || true

    # Building container with docker buildx
    docker buildx build \
        --platform="$BUILDX_BUILDER_ARCH" \
        --build-arg BASEIMAGE="$BASEIMAGE" \
        --build-arg IMAGE_VERSION="$IMAGE_VERSION" \
        --build-arg TARGETPLATFORM="$TARGETPLATFORM" \
        --build-arg JRIVER_RELEASE="$JRIVER_RELEASE" \
        --build-arg JRIVER_TAG="$JRIVER_TAG" \
        --build-arg APT_REPOSITORY="$APT_REPOSITORY" \
        --build-arg DEB_URL="$DEB_URL" $EXTRA_ARGS \
        --tag "$DOCKER_TAG" \
        --output type="docker" \
        .
}

# Build container with regular docker build
build_container() {
    DOCKER_TAG="$(get_docker_tag)"
    ARCH_TAG="$(get_arch_tag)"
    TARGETPLATFORM="$(get_targetplatform)"

    echo "Building image for platform $TARGETPLATFORM"

    DOCKER_TAG="$DOCKER_REPO:$BUILD_VARIANT-$IMAGE_VERSION-$ARCH_TAG"
    EXTRA_ARGS=""
    [ ${DISABLE_CACHE:-0} -eq 1 ] && EXTRA_ARGS="--no-cache --pull" || true

    # Building container with docker build
    docker build \
        --build-arg BASEIMAGE="$BASEIMAGE" \
        --build-arg IMAGE_VERSION="$IMAGE_VERSION" \
        --build-arg TARGETPLATFORM="$TARGETPLATFORM" \
        --build-arg JRIVER_RELEASE="$JRIVER_RELEASE" \
        --build-arg JRIVER_TAG="$JRIVER_TAG" \
        --build-arg APT_REPOSITORY="$APT_REPOSITORY" \
        --build-arg DEB_URL="$DEB_URL" $EXTRA_ARGS \
        -t "$DOCKER_TAG" \
        .
}

# Export built images to temporary registry
export_image() {
    DOCKER_TAG="$(get_docker_tag)"
    ARCH_TAG="$(get_arch_tag)"
    TEMP_IMAGE_NAME="$EXPORT_REGISTRY/jrivermc-docker"
    TEMP_IMAGE_TAG="$(uuidgen)"

    METADATA_FILE="$SCRIPT_DIR/buildinfo-$ARCH_TAG"

    # Export image
    echo "Exporting image as '$TEMP_IMAGE_NAME:$TEMP_IMAGE_TAG'"
    docker tag "$DOCKER_TAG" "$TEMP_IMAGE_NAME:$TEMP_IMAGE_TAG"

    docker push "$TEMP_IMAGE_NAME:$TEMP_IMAGE_TAG"

    # Export metadata
    [ -f "$METADATA_FILE" ] && rm "$METADATA_FILE" || true
    echo "DEB_URL=$DEB_URL" >> "$METADATA_FILE"
    echo "IMAGE_VERSION=$IMAGE_VERSION" >> "$METADATA_FILE"
    echo "TEMP_IMAGE_NAME=$TEMP_IMAGE_NAME" >> "$METADATA_FILE"
    echo "TEMP_IMAGE_TAG=$TEMP_IMAGE_TAG" >> "$METADATA_FILE"
    echo "RELEASE_REPO=$DOCKER_REPO" >> "$METADATA_FILE"
    echo "BUILD_VARIANT=$BUILD_VARIANT" >> "$METADATA_FILE"
}

print_build_info
if [ ${RUN_CLEANUP:-0} -eq 1 ]; then
    ./scripts/cleanup.sh "$DOCKER_REPO" "$EXPORT_REGISTRY/jrivermc-docker"
fi

if [ "${USE_BUILDX:-0}" -eq 1 ]; then 
    build_container_buildx
else
    build_container
fi

if [ "${EXPORT_IMAGE:-0}" -eq 1 ]; then
    export_image
fi

if [ ${RUN_CLEANUP:-0} -eq 1 ]; then
    ./scripts/cleanup.sh "$DOCKER_REPO" "$EXPORT_REGISTRY/jrivermc-docker"
fi
