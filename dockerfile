ARG BASEIMAGE

# Build jrivermc-docker-api
FROM mcr.microsoft.com/dotnet/sdk:9.0-bookworm-slim AS api-builder
# One of x86_64, arm, or armhf
ARG TARGETPLATFORM
ENV TARGETPLATFORM=${TARGETPLATFORM}
WORKDIR /app
ARG DEBIAN_FRONTEND=noninteractive
ARG COMMIT_SHA=5c06ed81d363b39d48189d30afbf236870398f67
RUN apt-get -q update && \
    apt-get -q install -y git clang zlib1g-dev && \
    git clone https://gitlab.com/shiomax/jrivermc-docker-api.git && \
    cd jrivermc-docker-api && \
    git -c advice.detachedHead=false checkout $COMMIT_SHA && \
    chmod +x publish.sh && \
    ./publish.sh

FROM public.ecr.aws/docker/library/node:20.11-bookworm AS web-builder
# JRiver Release Version (25, 26 etc.)
ARG JRIVER_RELEASE
WORKDIR /app
ARG COMMIT_SHA=eb3dd3af07df95f40492f22e4dd57f76c624b3d2
RUN apt-get -q update && \
    apt-get -q install -y git && \
    git clone https://gitlab.com/shiomax/jrivermc-docker-gui.git && \
    cd jrivermc-docker-gui && \
    git -c advice.detachedHead=false checkout $COMMIT_SHA && \
    npm i && \
    # Change application name
    echo "VITE_APP_NAME=JRiver Media Center ${JRIVER_RELEASE}" > .env && \
    npm run build

FROM ${BASEIMAGE}

# JRiver Version tag (latest, stable or beta)
ARG JRIVER_TAG

# JRiver Release Version (25, 26 etc.)
ARG JRIVER_RELEASE

# .deb download URL, if set to "repository" the JRiver repository will be used
ARG DEB_URL

# Image Version
ARG IMAGE_VERSION

# JRiver APT Repository to use (bullseye, buster, etc.)
ARG APT_REPOSITORY

# One of x86_64, arm, or armhf
ARG TARGETPLATFORM
# APT non interactive installs.
ARG DEBIAN_FRONTEND=noninteractive

ARG S6_OVERLAY_VERSION=3.2.0.2
ARG WEBSOCKIFY_VERSION=0.12.0
ARG PYJWT_VERSION=2.10.1
ARG NUMPY_VERSION=2.2.2

# Define working directory.
WORKDIR /tmp

# Copy helpers.
COPY helpers/* /usr/local/bin/
RUN chmod +x /usr/local/bin/add-pkg && \
    chmod +x /usr/local/bin/del-pkg && \
    chmod +x /usr/local/bin/sed-patch

# INSTALL S6-OVERLAY
RUN if [ $TARGETPLATFORM = linux/amd64 ]; then S6_ARCH=x86_64; \
    elif [ $TARGETPLATFORM = linux/arm64 ]; then S6_ARCH=arm; \
    elif [ $TARGETPLATFORM = linux/arm/v7 ]; then S6_ARCH=armhf; \
    else { echo "\033[0;31mUnsupported or unknown target platform '$TARGETPLATFORM'!\033[0m"; exit 1; } \
    fi && { \
        add-pkg wget ca-certificates xz-utils; \
        wget https://github.com/just-containers/s6-overlay/releases/download/v$S6_OVERLAY_VERSION/s6-overlay-noarch.tar.xz; \
        tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz; \
        wget https://github.com/just-containers/s6-overlay/releases/download/v$S6_OVERLAY_VERSION/s6-overlay-$S6_ARCH.tar.xz; \
        tar -C / -Jxpf /tmp/s6-overlay-$S6_ARCH.tar.xz; \
        del-pkg wget ca-certificates xz-utils; \
        [ -f /init ] || { echo "\033[0;31mError extracting s6-overlay-noarch.tar.xz!\033[0m"; exit 1; }; \
        [ -f /command/s6-overlay-suexec ] || { echo "\033[0;31mError extracting s6-overlay-$S6_ARCH.tar.xz!\033[0m"; exit 1; }; \
        rm -rf /tmp/* /tmp/.[!.]*; \
    }


# Setup nginx
RUN add-pkg nginx && \
    rm /etc/nginx/nginx.conf \
       /etc/init.d/nginx \
       /etc/logrotate.d/nginx \
       /etc/ufw/applications.d/nginx \
       /etc/default/nginx \
       && \
    rm -r /etc/nginx/snippets \
          /usr/share/nginx \
          /usr/share/doc/nginx \
          /var/log/nginx \
          /etc/nginx/sites-available/* \
          /etc/nginx/sites-enabled/* \
          /var/www/html/* \
          && \
    ln -s /log/nginx /var/log/nginx && \
    ln -s /tmp/nginx /var/lib/nginx && \
    # Create user under which nginx will run.
    useradd --system \
            --home-dir /dev/null \
            --no-create-home \
            --shell /sbin/nologin \
            nginx

# Install websockify
RUN add-pkg wget ca-certificates python3 python3-venv && \
    # Install websockify
    wget https://github.com/novnc/websockify/archive/refs/tags/v$WEBSOCKIFY_VERSION.tar.gz -O websockify.tar.gz && \
    mkdir /opt/websockify && \
    tar -xvzf websockify.tar.gz --strip-components=1 -C /opt/websockify && \
    # Remove uneeded files
    rm -r /opt/websockify/README.md \
        /opt/websockify/docs \
        /opt/websockify/Windows \
        /opt/websockify/tests \
        /opt/websockify/COPYING \
        /opt/websockify/CHANGES.txt \
        /opt/websockify/test-requirements.txt \
        /opt/websockify/rebind.c \
        /opt/websockify/rebind \
        /opt/websockify/Makefile \
        /opt/websockify/MANIFEST.in \
        /opt/websockify/tox.ini \
        && \
    # Create Python Venv
    python3 -m venv /opt/websockify/venv && \
    /opt/websockify/venv/bin/pip install pyjwt==${PYJWT_VERSION} && \
    /opt/websockify/venv/bin/pip install numpy==${NUMPY_VERSION} && \
    # Adjust Websockify run script to use Venv
    sed-patch s#python3#/opt/websockify/venv/bin/python3#g /opt/websockify/run && \
    # Create User for websockify
    useradd --system \
            --home-dir /dev/null \
            --no-create-home \
            --shell /sbin/nologin \
            websockify && \
    # Add NGINX user to websockify group for socket access
    usermod -aG websockify nginx && \ 
    # Cleanup
    del-pkg wget ca-certificates python3-venv && \
    rm -rf /tmp/* /tmp/.[!.]*

# Various packages
RUN add-pkg \
    # VNC Server
    tigervnc-standalone-server \
    # Desktop Environment
    jwm x11-utils \
    # Process Utils (pgrep etc.)
    procps \
    # Timezone Utils
    tzdata \
    # Additional fonts
    fonts-wqy-zenhei \
    # Window management tools for prevent-minimize script
    wmctrl xdotool \
    # SSL
    openssl \
    # ca-certs required for activation
    ca-certificates \
    # notifies when files change on disk 
    inotify-tools \
    # Not required, but it's dependencies are
    ffmpeg \
    # Required for chromium
    libgbm1 gnupg2 \
    # Required for browser on arm (automatically installed on amd64)
    libnss3 && \
    # MC 32 requires webkit for internal browser to work, even though it theoretically is only used on 32bit installs
    if [ $JRIVER_RELEASE -ge 32 ]; then add-pkg libwebkit2gtk-4.0-37; fi

# Install JRiver
RUN \
    add-pkg wget && \
    # Install from Repository
    if [ "${DEB_URL}" = "repository" ]; then \
        echo "Installing JRiver from repository ${JRIVER_RELEASE}:${JRIVER_TAG}" && \
        wget -qO- "http://dist.jriver.com/mediacenter@jriver.com.gpg.key" -O- | gpg --dearmor | tee /etc/apt/trusted.gpg.d/jriver.gpg && \
        echo "deb [trusted=yes arch=amd64,i386,armhf,arm64] http://dist.jriver.com/${JRIVER_TAG}/mediacenter/ ${APT_REPOSITORY} main" >> /etc/apt/sources.list.d/mediacenter.list && \
        apt-get update && \
        add-pkg mediacenter${JRIVER_RELEASE}; \
    # Install from .deb URL
    else \
        echo "Installing JRiver from URL: ${DEB_URL}" && \
        wget -q -O "jrivermc.deb" ${DEB_URL} && \
        add-pkg "./jrivermc.deb"; \
    fi && \
    # Cleanup
    del-pkg wget && \
    rm -rf /tmp/* /tmp/.[!.]*

# Add files.
COPY rootfs/ /

# Copy jrivermc-docker-api
COPY --from=api-builder /app/jrivermc-docker-api/dist /var/www/html/api
# Copy jrivermc-docker-gui
COPY --from=web-builder /app/jrivermc-docker-gui/dist /var/www/html/web

# Set permissions etc. on rootfs files, etc.
RUN \
    chmod +x /etc/s6-overlay/s6-rc.d/setup-*/run && \
    chmod +x /usr/bin/mcd-stop && \
    chmod +x /usr/bin/mcd-activate && \
    chmod +x /usr/bin/mcd-start && \
    chown -R websockify:websockify /opt/websockify && \
    # Replace %MC_VERSION% with current MC version
    sed-patch s/%%MC_VERSION%%/${JRIVER_RELEASE}/g \
        /etc/s6-overlay/s6-rc.d/jriver/run && \
    sed-patch s/%%MC_VERSION%%/${JRIVER_RELEASE}/g \
        /usr/bin/mcd-stop && \
    sed-patch s/%%MC_VERSION%%/${JRIVER_RELEASE}/g \
        /usr/bin/mcd-start && \
    sed-patch s/%%MC_VERSION%%/${JRIVER_RELEASE}/g \
        /usr/bin/mcd-activate && \
    sed-patch s/%%MC_VERSION%%/${JRIVER_RELEASE}/g \
        /etc/s6-overlay/s6-rc.d/prevent-minimize/run && \
    # Write Version file
    echo "IMAGE_VERSION ${IMAGE_VERSION}" > /.imgversion && \
    echo "DEB_URL ${DEB_URL}" >> /.imgversion && \
    echo "JRIVER_TAG ${JRIVER_TAG}" >> /.imgversion && \
    chmod 444 /.imgversion
    

# Set environment variables.
ENV XDG_DATA_HOME=/config/xdg/data \
    XDG_CONFIG_HOME=/config/xdg/config \
    XDG_CACHE_HOME=/config/xdg/cache \
    XDG_RUNTIME_DIR=/tmp/run/user/app \
    CLEAN_TMP_DIR=1 \
    SECURE_CONNECTION=1 \
    TAKE_CONFIG_OWNERSHIP=1 \
    APP_USER=app \
    USER_ID=1000 \
    GROUP_ID=1000 \
    DISPLAY=:0 \
    NVIDIA_DRIVER_CAPABILITIES=all \
    NVIDIA_VISIBLE_DEVICES=all \
    STATELESS_MODE=0 \
    DISPLAY_WIDTH=1280 \
    DISPLAY_HEIGHT=768 \
    S6_CMD_WAIT_FOR_SERVICES_MAXTIME=0 \
    CERTMONITOR_INTERVAL=604800 \
    MC_STARTUP_OPTIONS="/MediaServer"

# Define mountable directories. 
VOLUME ["/config"]

EXPOSE 5800 5900 52100 52101 52199 1900/udp

# Metadata.
LABEL \
      org.label-schema.name="jrivermc" \
      org.label-schema.description="Docker container for JRiver MediaCenter" \
      org.label-schema.version="${IMAGE_VERSION}" \
      org.label-schema.vcs-url="https://gitlab.com/shiomax/jrivermc-docker" \
      org.label-schema.schema-version="1.0"

ENTRYPOINT ["/init"]
