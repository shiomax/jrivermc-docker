#!/bin/bash

set -u # Treat unset variables as an error.

# Image names
AMD64_IMAGE_NAME="$(./scripts/readvar.sh amd64 TEMP_IMAGE_NAME)"
ARM64_IMAGE_NAME="$(./scripts/readvar.sh amd64 TEMP_IMAGE_NAME)"

# Image tags
AMD64_IMAGE_TAG="$(./scripts/readvar.sh amd64 TEMP_IMAGE_TAG)"
ARM64_IMAGE_TAG="$(./scripts/readvar.sh amd64 TEMP_IMAGE_TAG)"

echo "Will remove the following images..."
echo "AMD64_IMAGE: $AMD64_IMAGE_NAME:$AMD64_IMAGE_TAG"
echo "ARM64_IMAGE: $ARM64_IMAGE_NAME:$ARM64_IMAGE_TAG"

get_gitlab_repository_id() {
    curl -s \
        --request GET \
        "$CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/registry/repositories" | \
        jq --arg loc "$1" '.[] | first(select(.location == $loc)) | .id'
}

gitlab_remove_tag() {
    curl -s \
        --request DELETE \
        --header "PRIVATE_TOKEN: $CI_JOB_TOKEN" \
        "$CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/registry/repositories/$1/tags/$2"
}

# Remove images from gitlab registry
cleanup_gitlab_registry() {
    echo "Removing amd64 image..."
    gitlab_remove_tag "$(get_gitlab_repository_id "$AMD64_IMAGE_NAME")" "$AMD64_IMAGE_TAG"
    echo "Removing arm64 image..."
    gitlab_remove_tag "$(get_gitlab_repository_id "$ARM64_IMAGE_NAME")" "$ARM64_IMAGE_TAG"
}

# Remove images from self hosted registry
cleanup_docker_registry() {
    echo "Removing amd64 image..."
    ./scripts/remove-registry-tag.sh --image-name jrivermc-docker --tag "$AMD64_IMAGE_TAG" --registry "$CI_TEMP_REGISTRY"
    echo "Removing arm64 image..."
    ./scripts/remove-registry-tag.sh --image-name jrivermc-docker --tag "$ARM64_IMAGE_TAG" --registry "$CI_TEMP_REGISTRY"
}

if [ "$CI_TEMP_REGISTRY" = "registry.gitlab.com" ]; then
    cleanup_gitlab_registry
else
    cleanup_docker_registry
fi
