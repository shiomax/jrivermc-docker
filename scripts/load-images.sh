#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

ARCH=ALL

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        --arch)
            shift
            ARCH="$1"
            ;;
        -*)
            echo "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

# Validate if all expected files exist
if [ "$ARCH" = "ALL" ] || [ "$ARCH" = "amd64" ]; then
    [ -f buildinfo-amd64 ] || { "File 'buildinfo-amd64' is missing."; exit 1; }
fi

if [ "$ARCH" = "ALL" ] || [ "$ARCH" = "arm64" ]; then
    [ -f buildinfo-arm64 ] || { "File 'buildinfo-arm64' is missing."; exit 1; }
fi

# Load images
if [ "$ARCH" = "ALL" ] || [ "$ARCH" = "amd64" ]; then
    echo "Loading image for arch amd64..."
    docker pull "$(./scripts/readvar.sh amd64 TEMP_IMAGE_FULL_NAME)"
fi

if [ "$ARCH" = "ALL" ] || [ "$ARCH" = "arm64" ]; then
    echo "Loading image for arch arm64..."
    docker pull "$(./scripts/readvar.sh arm64 TEMP_IMAGE_FULL_NAME)"
fi
