#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

usage() {
    echo "usage: $( basename $0 ) <docker repo(s)>"
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

for DOCKER_REPO in "$@"
do
    docker images | while read line; do
        if [ "$(echo $line | awk '{print $1}')" = "REPOSITORY" ]; then
            continue
        fi

        REPOSITORY="$(echo $line | awk '{print $1}')"

        if [ "$REPOSITORY" = "<none>" ] || [ "$REPOSITORY" = "$DOCKER_REPO" ]; then
            IMAGE_ID="$(echo $line | awk '{print $3}')"

            docker ps -a | while read psline; do
                if [ "$(echo $line | awk '{print $1}')" = "CONTAINER ID" ]; then
                    continue
                fi

                CONTAINER_ID="$(echo $psline | awk '{print $1}')"
                CONTAINER_IMAGE_ID="$(echo $psline | awk '{print $2}')"
                CONTAINER_REPO="$(echo "$CONTAINER_IMAGE_ID" | awk -F: '{print $1}')"

                # Remove containers that use that image or the repository
                if [ "$CONTAINER_IMAGE_ID" = "$IMAGE_ID" ] || [ "$CONTAINER_REPO" = "$REPOSITORY" ]; then
                    echo "Removing container: $CONTAINER_ID using image: $CONTAINER_IMAGE_ID"
                    docker rm -f "$CONTAINER_ID" &> /dev/null || true
                fi
            done

            echo "Removing image: $IMAGE_ID from repo: $REPOSITORY"
            docker image rm --force $IMAGE_ID &> /dev/null || true
        fi
    done
done

echo "Cleaning up build cache..."
docker builder prune -f
