#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

# Script reads various variables used in test / publish stages
# ./readvar.sh <amd64 / arm64> <variable>
# IMAGE_VERSION ... version number of the image
# JRIVER_TAG ... latest or stable
# DEB_URL ... repository or the download url for .deb file
# IS_LATEST ... 1 or 0 wether or not latest or stable tags should be pushed for this image
# REPOSITORY ... dockerhub release repository
# JRIVER_RELEASE ... major version of MC 28/29 etc.
# TEMP_IMAGE_NAME ... Image name
# TEMP_IMAGE_TAG ... Image tag
# TEMP_IMAGE_FULL_NAME ... TEMP_IMAGE_NAME:TEMP_IMAGE_TAG

ARCH="$1"
VARIABLE="$2"

# $1 = ARCH
buildinfo() {
    cat "./buildinfo-$1"
}

# Prints out content of selected version based on buildvariant file written in build.sh
# $1 = ARCH
buildvariant() {
    BUILD_VARIANT="$(cat ./buildinfo-$1 | grep "^BUILD_VARIANT=.*$" | awk -F= '{print $2}')"
    cat "./versions/$BUILD_VARIANT"
}

# Check if version is latest mc version available.
# $1 = ARCH
islatest() {
    LATEST_VERSION="$(ls -l ./versions | tail -n +2 | awk '{print $9}' | sort --reverse --sort=general-numeric | head -n 1)"
    CURRENT_VERSION="mc$(buildvariant "$1" | grep "^JRIVER_RELEASE=.*$" | awk -F= '{print $2}')"
    [ "$CURRENT_VERSION" = "$LATEST_VERSION" ] && echo "1" || echo "0"
}

# Check if version is latest image where stable mc version is set to enabled.
# Will only check against latest 3 versions.
# $1 = ARCH
isLatestStable() {
    CURRENT_VERSION="mc$(buildvariant "$1" | grep "^JRIVER_RELEASE=.*$" | awk -F= '{print $2}')"

    while read version; do
        STABLE_AVAILABLE="$(cat ./versions/$version | grep "^STABLE_AVAILABLE=.*$" | awk -F= '{print $2}')"
        if [ "$STABLE_AVAILABLE" = "1" ]; then
            [ "$version" = "$CURRENT_VERSION" ] && echo "1" || echo "0"
            return 0
        fi
    done < <(ls -l ./versions | tail -n +2 | awk '{print $9}' | sort --reverse --sort=general-numeric | head -n 3)

    # default value
    echo "0"
}

# $1 = ARCH
readvar() {
    case "$VARIABLE" in
        IMAGE_VERSION | TEMP_IMAGE_NAME | TEMP_IMAGE_TAG | DEB_URL | RELEASE_REPO)
            buildinfo "$1" | grep "^$VARIABLE=.*$" | awk -F= '{print $2}'
            ;;
        JRIVER_RELEASE | STABLE_AVAILABLE | APT_REPOSITORY)
            buildvariant "$1" | grep "^$VARIABLE=.*$" | awk -F= '{print $2}'
            ;;
        TEMP_IMAGE_FULL_NAME)
            NAME="$(buildinfo "$1" | grep "^TEMP_IMAGE_NAME=.*$" | awk -F= '{print $2}')"
            TAG="$(buildinfo "$1" | grep "^TEMP_IMAGE_TAG=.*$" | awk -F= '{print $2}')"
            echo "$NAME:$TAG"
            ;;
        IS_LATEST)
            islatest "$1"
            ;;
        IS_LATEST_STABLE)
            isLatestStable "$1"
            ;;
        *)
            echo "Unsupported variable $VARIABLE."
            exit 1
            ;;
    esac
}

if [ "$ARCH" = "all" ]; then
    AMD64="$(readvar amd64)"
    ARM64="$(readvar arm64)"

    [ "$AMD64" = "$ARM64" ] || { echo "Variables do not match for all architectures."; exit 1; }

    echo "$AMD64"
else
    echo "$(readvar "$ARCH")"
fi


